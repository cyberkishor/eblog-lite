<?php
/**
 * eBlog Lite functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eBlog Lite
 */

if( !function_exists('eblog_lite_file_directory') ){

    function eblog_lite_file_directory( $file_path ){
        if( file_exists( trailingslashit( get_stylesheet_directory() ) . $file_path) ) {
            return trailingslashit( get_stylesheet_directory() ) . $file_path;
        }
        else{
            return trailingslashit( get_template_directory() ) . $file_path;
        }
    }
}
/**
* Customizer
**/
require eblog_lite_file_directory('spiderbuzz/customizer.php');

require eblog_lite_file_directory('spiderbuzz/KCustomizer.php');


/**
* Hooks File
**/
require eblog_lite_file_directory('spiderbuzz/hooks/eblog-hooks.php');
