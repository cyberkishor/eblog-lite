<?php
    /**
    * eBlog Lite Theme Customizer
    *
    * @package eBlog Lite
    */

    /**
    * Add postMessage support for site title and description for the Theme Customizer.
    *
    * @param WP_Customize_Manager $wp_customize Theme Customizer object.
    */

    class eblogliteCustomizer{
        function __construct(){
            add_action( 'customize_register', array($this,'eblog_lite_general_customize') );
            
            
            add_action('customize_register',array($this,'eblog_lite_social_links_customizer'));
    
        }
        function __destruct() {
            $vars = array_keys(get_defined_vars());
            for ($i = 0; $i < sizeOf($vars); $i++) {
                unset($vars[$i]);
            }
            unset($vars,$i);
        }
        public static function get_instance() {
            static $instance;
            $class = __CLASS__;
            if( ! $instance instanceof $class) {
                $instance = new $class;
            }
            return $instance;
        }

        function eblog_lite_general_customize( $wp_customize ) {
            $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
            $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
            $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

            if ( isset( $wp_customize->selective_refresh ) ) {
                $wp_customize->selective_refresh->add_partial( 'blogname', array(
                    'selector'        => '.site-title a',
                    'render_callback' => 'eblog_lite_customize_partial_blogname',
                ) );
                $wp_customize->selective_refresh->add_partial( 'blogdescription', array(
                    'selector'        => '.site-description',
                    'render_callback' => 'eblog_lite_customize_partial_blogdescription',
                ) );
            }
            /**
            * General Settings Panel
            */
            $wp_customize->add_panel('eblog_lite_general_settings', array(
                'priority' => 3,
                'title' => esc_html__('General Settings', 'eblog-lite')
            ));

            $wp_customize->get_section('title_tagline')->panel = 'eblog_lite_general_settings';
            $wp_customize->get_section('title_tagline' )->priority = 1;

            $wp_customize->get_section('header_image')->panel = 'eblog_lite_general_settings';
            $wp_customize->get_section('header_image' )->priority = 2;

            $wp_customize->get_section('colors')->panel = 'eblog_lite_general_settings';
            $wp_customize->get_section('background_image')->panel = 'eblog_lite_general_settings';
            $wp_customize->get_section('header_image' )->priority = 4;

        }
        //Social Links
        function eblog_lite_social_links_customizer($wp_customize) {
            $customizer = KCustomizer::get_instance($wp_customize);
            $default = array(
                'section' => array(
                        'id'        => "eblog_lite_social_links",
                        'label'     => __("Social Links", 'eblog-lite'),
                        'priority'  => 4
                    ),
                    'fields' => array(
                        array(
                            // for settigns
                            'default'   => true,
                            'transport' => 'refresh',
                            //for control
                            'id'    => "eblog_lite_social_links_enable",
                            'type'  => 'checkbox',
                            'label' => __("Enabel", 'eblog-lite')
                        ),
                        array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "facebook_url",
                                'type'  => 'url',
                                'label' => __("Facebook URL", 'eblog-lite')
                            ),
                            array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "google_plus",
                                'type'  => 'url',
                                'label' => __("Google Plus", 'eblog-lite')
                            ),
                            array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "twitter_url",
                                'type'  => 'url',
                                'label' => __("Twitter URL", 'eblog-lite')
                            ),
                            array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "rss_url",
                                'type'  => 'url',
                                'label' => __("RSS URL", 'eblog-lite')
                            ),
                            array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "linkedin_url",
                                'type'  => 'url',
                                'label' => __("Linkedin URL", 'eblog-lite')
                            ),
                            array(
                                // for settigns
                                'default'   => "",
                                'transport' => 'postMessage',
                                //for control
                                'id'    => "instagram_url",
                                'type'  => 'url',
                                'label' => __("Instagram URL", 'eblog-lite')
                            ),
                            
                    )
                );
            $customizer->prepare( $default );
        }
  
    }
eblogliteCustomizer::get_instance();