<?php


/**
 * eBlog Lite  Social Links
 */
 if ( ! function_exists( 'eblog_lite_social_links' ) ) {
	function eblog_lite_social_links( ) {	
    $facebook_url = get_theme_mod('facebook_url');
    $googleplus_url = get_theme_mod('google_plus');
    $twitter_url = get_theme_mod('twitter_url');
    $rss_url = get_theme_mod('rss_url');
    $linkedin_url = get_theme_mod('linkedin_url');
    $instagram = get_theme_mod('instagram_url');

    $social_links_enable= get_theme_mod('eblog_lite_social_links_enable');
    if($social_links_enable == true){
    ?>
    <ul class="inline-mode">
        <?php if(!empty( $facebook_url) ) { ?><li class="social-network fb"><a title="<?php esc_attr('Connect us on Facebook','eblog-lite') ?>" target="_blank" href="<?php echo esc_url($facebook_url);  ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
        <?php if(!empty( $googleplus_url) ) { ?><li class="social-network googleplus"><a title="<?php esc_attr('Connect us on Google+','eblog-lite'); ?>" target="_blank" href="<?php echo esc_url($googleplus_url); ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
        <?php if(!empty( $twitter_url) ) { ?><li class="social-network tw"><a title="<?php esc_attr('Connect us on Twitter','eblog-lite'); ?>" target="_blank" href="<?php echo esc_url($twitter_url);  ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
        <?php if(!empty( $rss_url) ) { ?><li class="social-network rss"><a title="<?php esc_attr('Connect us on Instagram','eblog-lite'); ?>" target="_blank" href="<?php echo esc_url($rss_url);  ?>"><i class="fa fa-rss"></i></a></li><?php } ?>
        <?php if(!empty( $linkedin_url) ) { ?><li class="social-network linkedin"><a title="<?php esc_attr('Connect us on Linkedin','eblog-lite'); ?>" target="_blank" href="<?php echo esc_url($linkedin_url); ?>"><i class="fa fa-linkedin"></i></a></li><?php } ?>
        <?php if(!empty( $instagram) ) { ?><li class="social-network instagram"><a title="<?php esc_attr('Connect us on Instagram','eblog-lite'); ?>" target="_blank" href="<?php echo esc_url($instagram);  ?>"><i class="fa fa-instagram"></i></a></li><?php } ?>
    </ul>    
	<?php }
	}
}
add_action( 'eblog_lite_footer_social_links', 'eblog_lite_social_links');