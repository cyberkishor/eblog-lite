=== eBlog Lite ===

Contributors: spiderbuzz
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready,blog

Requires at least: 4.0
Tested up to: 4.8.1
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called eBlog Lite.

== Description ==

eBlog Lite is modern, clean, colorful and responsive blog theme. It can be used for blogs, informative, news and other creative websites. It uses an amazing WordPress Customizer for theme options.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

eBlog Lite includes support for Infinite Scroll in Jetpack.


== Changelog ==
= 1.0 - oct 4 2017 =
* Initial release



== License ==
The exceptions to license are as follows:

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv3 or later](http://www.gnu.org/licenses/gpl-3.0.txt)

Font Awesome 4.5.0
Created by @davegandy, http://fontawesome.io - @fontawesome
MIT License: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)


Bootstrap v3.3.4 (http://getbootstrap.com)
Copyright 2011-2015 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

Image used in screenshot
https://cdn.pixabay.com/photo/2016/07/27/17/56/girl-1545885_960_720.jpg             License CC0 Public Domain
https://cdn.pixabay.com/photo/2014/10/03/08/33/nature-471179_960_720.jpg             License CC0 Public Domain


== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
